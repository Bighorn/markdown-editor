#include "mdformatter.h"

MdFormatter::MdFormatter()
{

}

void MdFormatter::emphasisText(QTextCursor &cursor, bool isStrong)
{
    QString * tag;
    if(isStrong){
        tag = new QString("**");
    } else {
        tag = new QString("*");
    }

    int userSelectionStart = cursor.selectionStart();
    int userSelectionEnd = cursor.selectionEnd();

    bool hasChars;

    cursor.setPosition(userSelectionStart);
    hasChars = cursor.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, tag->length());
    bool strongStart = false;
    if(hasChars){
        strongStart = cursor.selectedText() == tag;
    }

    cursor.setPosition(userSelectionEnd);
    hasChars = cursor.movePosition(QTextCursor::Right, QTextCursor::KeepAnchor, tag->length());
    bool strongEnd = false;
    if(hasChars){
        strongEnd = cursor.selectedText() == tag;
    }

    QString updatedText;
    if(strongStart && strongEnd){
        cursor.setPosition(userSelectionStart-(tag->length()));
        cursor.setPosition(userSelectionEnd+(tag->length()), QTextCursor::KeepAnchor);
        updatedText = cursor.selectedText().remove(0,tag->length()).remove(-(tag->length()),tag->length());
    } else {
        cursor.setPosition(userSelectionStart);
        cursor.setPosition(userSelectionEnd, QTextCursor::KeepAnchor);
        updatedText = cursor.selectedText().prepend(tag).append(tag);
    }

     cursor.insertText(updatedText);
}

void MdFormatter::formatStrong(QTextCursor &cursor)
{
    emphasisText(cursor, true);
}

void MdFormatter::formatEmphasis(QTextCursor &cursor)
{
    emphasisText(cursor);
}
