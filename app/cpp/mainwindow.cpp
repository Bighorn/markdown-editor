#include "mainwindow.h"
#include "ui_mainwindow.h"

const QString MainWindow::APP_TITLE = "Markdown Editor";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    editorCanvas = ui->centralWidget->findChild<QPlainTextEdit *>("editorCanvas");
    setWindowTitle(APP_TITLE+" - "+"Untitled");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::fileSave(bool existingSave)
{
    if(!existingSave) {
        if(file) {
            file->close();
            delete file;
        }

        QString filename = QFileDialog::getSaveFileName(
            this,
            tr("Save File"),
            QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
            "Markdown (*.md)"
        );

        file = new QFile(filename);
        setWindowTitle(APP_TITLE+" - "+file->fileName());
    }

    if(!file->isOpen()){
        if(!file->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) return;
    }

    QTextStream * out = new QTextStream(file);
    *out << editorCanvas->toPlainText();
    out->flush();
    delete out;
    file->close();
    editorCanvas->document()->setModified(false);
}

void MainWindow::unsavedChanges(std::function<void()> cb)
{
    if(editorCanvas->document()->isModified()){
        QMessageBox messageBox;
        messageBox.setText("There are unsaved changes. Save before exiting?");
        messageBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        messageBox.setDefaultButton(QMessageBox::Save);
        int answer = messageBox.exec();

        switch (answer) {
            case QMessageBox::Save:
                fileSave(file);
                cb();
                break;
            case QMessageBox::Discard:
                cb();
                break;
            case QMessageBox::Cancel:
                return;
            default:
                break;
        }
    } else {
        cb();
    }
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    unsavedChanges([](){});
    QMainWindow::closeEvent(event);
}

void MainWindow::on_actionNew_triggered()
{
    auto cb = [this] () {
        delete file;
        editorCanvas->clear();
    };

    unsavedChanges(cb);
}

void MainWindow::on_actionOpen_triggered()
{
    auto cb = [this] () {
        QString filename = QFileDialog::getOpenFileName(
            this,
            tr("Open File"),
            QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
            "Markdown (*.md)"
        );

        delete file;
        file = new QFile(filename);
        if(!file->open(QIODevice::ReadWrite | QIODevice::Text)) return;
        QTextStream * in = new QTextStream(file);
        QString content = in->readAll();
        delete in;
        file->close();

        editorCanvas->setPlainText(content);
        editorCanvas->document()->setModified(false);
        setWindowTitle(APP_TITLE+" - "+file->fileName());
    };

    unsavedChanges(cb);
}

void MainWindow::on_actionSave_triggered()
{
    fileSave(file);
}

void MainWindow::on_actionSave_As_triggered()
{
    fileSave(false);
}

void MainWindow::on_editorCanvas_modificationChanged(bool changed)
{
    ui->actionSave->setEnabled(changed);
}

void MainWindow::on_actionBold_triggered()
{
    QTextCursor cursor = editorCanvas->textCursor();
    MdFormatter::formatStrong(cursor);
}

void MainWindow::on_actionItalic_triggered()
{
    QTextCursor cursor = editorCanvas->textCursor();
    MdFormatter::formatEmphasis(cursor);
}
