#-------------------------------------------------
#
# Project created by QtCreator 2018-08-25T18:25:05
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = markdown-editor
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11


SOURCES += cpp/main.cpp \
    cpp/mainwindow.cpp \
    cpp/mdformatter.cpp

HEADERS  += ../include/mainwindow.h \
    ../include/mdformatter.h

FORMS    += ui/mainwindow.ui

INCLUDEPATH+= ../include

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc
