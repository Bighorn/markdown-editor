#include "mdformatter.h"
#include <QtTest>
#include <QTextDocument>
#include <QTextCursor>
// add necessary includes here

class MdFormatterTest : public QObject
{
    Q_OBJECT

public:
    MdFormatterTest();
    ~MdFormatterTest();

private slots:
    void init();
    void formatStrong();
    void unformatStrong();
    void formatEmphasis();
    void unformatEmphasis();
    void cleanup();

private:
    QTextDocument * doc;
    QTextCursor * cursor;
};

MdFormatterTest::MdFormatterTest()
{

}

MdFormatterTest::~MdFormatterTest()
{

}

void MdFormatterTest::init()
{
    doc = new QTextDocument();
    cursor = new QTextCursor(doc);
}

void MdFormatterTest::cleanup()
{
    delete cursor;
    delete doc;
}

void MdFormatterTest::formatStrong()
{
    cursor->insertText("A");
    cursor->select(QTextCursor::LineUnderCursor);

    MdFormatter::formatStrong(*cursor);
    cursor->select(QTextCursor::LineUnderCursor);
    QCOMPARE(cursor->selectedText(), QString("**A**"));
}

void MdFormatterTest::unformatStrong()
{
    cursor->insertText("**A**");
    cursor->setPosition(2);
    cursor->movePosition(QTextCursor::Right, QTextCursor::KeepAnchor);

    MdFormatter::formatStrong(*cursor);
    cursor->select(QTextCursor::LineUnderCursor);
    QCOMPARE(cursor->selectedText(), QString("A"));
}

void MdFormatterTest::formatEmphasis()
{
    cursor->insertText("A");
    cursor->select(QTextCursor::LineUnderCursor);

    MdFormatter::formatEmphasis(*cursor);
    cursor->select(QTextCursor::LineUnderCursor);
    QCOMPARE(cursor->selectedText(), QString("*A*"));
}

void MdFormatterTest::unformatEmphasis()
{
    cursor->insertText("*A*");
    cursor->setPosition(1);
    cursor->movePosition(QTextCursor::Right, QTextCursor::KeepAnchor);

    MdFormatter::formatEmphasis(*cursor);
    cursor->select(QTextCursor::LineUnderCursor);
    QCOMPARE(cursor->selectedText(), QString("A"));
}

QTEST_APPLESS_MAIN(MdFormatterTest)

#include "tst_class.moc"
