QT += testlib
QT += gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_class.cpp
TARGET = mdformattertest

HEADERS  += ../../include/mdformatter.h
INCLUDEPATH+= ../../include ../../build/app
LIBS += $$PWD/../../build/app/debug/mdformatter.o
