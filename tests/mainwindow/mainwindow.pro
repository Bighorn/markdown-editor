QT += testlib
QT += gui
QT += widgets
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

SOURCES +=  tst_class.cpp
TARGET = test

HEADERS  += ../../include/mainwindow.h
INCLUDEPATH+= ../../include ../../build/app
LIBS += $$PWD/../../build/app/debug/mainwindow.o \
$$PWD/../../build/app/debug/mdformatter.o
