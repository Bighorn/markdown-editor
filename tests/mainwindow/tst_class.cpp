#include <QtTest>
#include <QCoreApplication>
#include <QAction>
#include "mainwindow.h"

// add necessary includes here

class MainWindowTest : public QObject
{
    Q_OBJECT

public:
    MainWindowTest();
    ~MainWindowTest();
private:
    MainWindow * instance;

private slots:
    void init();
    void cleanup();
    void noSaveUnmodified();

};

MainWindowTest::MainWindowTest()
{

}

MainWindowTest::~MainWindowTest()
{

}

void MainWindowTest::init()
{
    instance = new MainWindow;
}

void MainWindowTest::cleanup()
{
    delete instance;
}

void MainWindowTest::noSaveUnmodified()
{
    instance->show();
    QPlainTextEdit * editorCanvas = instance->findChild<QPlainTextEdit *>("editorCanvas");
    QAction * actionSave = instance->findChild<QAction *>("actionSave");

    QVERIFY(!(actionSave->isEnabled()));

    QTest::keyClicks(editorCanvas, "Hello World");

    QVERIFY(actionSave->isEnabled());
}

QTEST_MAIN(MainWindowTest)

#include "tst_class.moc"
