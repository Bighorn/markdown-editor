#ifndef DUMMY_H
#define DUMMY_H
#include <QTextCursor>

class MdFormatter
{
public:
    MdFormatter();
    static void formatStrong(QTextCursor &cursor);
    static void formatEmphasis(QTextCursor &cursor);
private:
    static void emphasisText(QTextCursor &cursor, bool isStrong = false);
};

#endif // DUMMY_H
