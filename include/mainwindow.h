#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mdformatter.h"
#include <QMainWindow>
#include <QPlainTextEdit>
#include <QFileDialog>
#include <QStandardPaths>
#include <QTextStream>
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void fileSave(bool existingSave = true);
    void unsavedChanges(std::function<void()> cb);

private slots:
    void on_actionExit_triggered();

    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_editorCanvas_modificationChanged(bool changed);

    void on_actionBold_triggered();

    void on_actionItalic_triggered();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::MainWindow *ui;
    QFile * file = nullptr;
    QPlainTextEdit * editorCanvas = nullptr;
    static const QString APP_TITLE;
};

#endif // MAINWINDOW_H
